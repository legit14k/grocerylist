﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrimbergJason_GroceryList
{
    public partial class MainForm : Form
    {
        // --------------------------------------------------------------------------------------------------
        // EventHandler call
        // --------------------------------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler ObjectToNeed;
        public EventHandler ObjectToHave;
        public EventHandler SelectionChanged;
        
        public MainForm()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------------------
        // Triggers
        // --------------------------------------------------------------------------------------------------
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Make all buttons not usable
            btnDelete.Enabled = false;
            btnToHave.Enabled = false;
            btnToNeed.Enabled = false;
            
            // Sub to the to have handler
            ObjectToHave += ObjectToHaveHandler;

            // Sub to the to need handler
            ObjectToNeed += ObjectToNeedHandler;
        }
        
        // New item menu call
        private void menuNew_Click(object sender, EventArgs e)
        {
            // New instance of the user input window
            UserInputDialog uid = new UserInputDialog();

            // Sub to the event handler for the object added
            uid.ObjectAdded = new EventHandler(this.ObjectAddedHandler);

            // Show a new user input window
            uid.ShowDialog();
        }

        // Button to need
        private void btnToNeed_Click(object sender, EventArgs e)
        {
            // Check to make sure something is selected
            if (lvHave.SelectedItems.Count > 0)
            {
                // Check to make sure there is not null
                if (ObjectToNeed != null)
                {
                    ObjectToNeed(this, new EventArgs());
                }
            }
        }

        // Button to have
        private void btnToHave_Click(object sender, EventArgs e)
        {
            // Check to make sure something is selected
            if (lvNeed.SelectedItems.Count > 0)
            {
                // Check to make sure there is not null
                if (ObjectToHave != null)
                {
                    ObjectToHave(this, new EventArgs());
                }
            }
        }

        // Button to delete the current selection
        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Check to make sure something is selected
            if (lvNeed.SelectedItems.Count > 0)
            {
                // Deletes which ever item is selected in need list
                foreach (ListViewItem i in lvNeed.SelectedItems)
                {
                    lvNeed.Items.Remove(i);
                }

            }
            // Check to make sure something is selected
            else if (lvHave.SelectedItems.Count > 0)
            {
                // Deletes which ever item is selected in have list
                foreach (ListViewItem i in lvHave.SelectedItems)
                {
                    lvHave.Items.Remove(i);
                }
            }
            // Message box to show what the user is doing wrong
            else
            {
                MessageBox.Show("Please select an item to delete.");
                return;
            }
        }
        
        // When an item is selected in the have list
        private void lvHave_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectionChange();
        }

        // When an item is selected in the need list
        private void lvNeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectionChange();
        }

        // Load menu item trigger
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call load file method
            LoadFile();
        }

        // Save menu item trigger
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Call the save file method
            SaveFile();
        }

        // Exit menu item trigger
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Close out of the application
            Application.Exit();
        }

        // --------------------------------------------------------------------------------------------------
        // Object Handlers
        // --------------------------------------------------------------------------------------------------
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the main form as the sender
            UserInputDialog uid = sender as UserInputDialog;

            // Set the items values to new data
            Items i = uid.Data;

            // Set a new instance of the list view item
            ListViewItem lvi = new ListViewItem();

            // Set the list view item as string
            lvi.Text = i.ToString();

            // Set the new list view item tag as the string
            lvi.Tag = i;

            // Check which radio button is selected 
            if (uid.radToHave.Checked == true)
            {
                // Add new object/item to the have list
                lvHave.Items.Add(lvi);
            }
            else if (uid.radToNeed.Checked == true)
            {
                // Add new object/item to the need list
                lvNeed.Items.Add(lvi);
            }
            else
            {
                // Tell the user what is going on
                MessageBox.Show("This item has nowhere to go.");
            }
        }

        // To have from need object handler 
        public void ObjectToHaveHandler(object sender, EventArgs e)
        {
            // Set new clone item
            ListViewItem itemClone;

            // Set the clone variable with the current selection
            itemClone = lvNeed.SelectedItems[0].Clone() as ListViewItem;

            // Remove the old variable
            lvNeed.Items.Remove(lvNeed.SelectedItems[0]);

            // Add the clone to the new list
            lvHave.Items.Add(itemClone);

            // Reset all of the inputs
            ResetInputs();
        }

        // To need from have object handler
        public void ObjectToNeedHandler(object sender, EventArgs e)
        {
            // Set new clone item
            ListViewItem itemClone;
            
            // Set the clone variable with the current selection
            itemClone = lvHave.SelectedItems[0].Clone() as ListViewItem;

            // Remove the old variable
            lvHave.Items.Remove(lvHave.SelectedItems[0]);

            // Add the clone to the new list
            lvNeed.Items.Add(itemClone);

            // Reset all of the inputs
            ResetInputs();
        }
        
        // --------------------------------------------------------------------------------------------------
        // Save file method
        // --------------------------------------------------------------------------------------------------
        public void SaveFile()
        {
            // Use the SaveFileDialog to open a save dialog box
            using (var sfd = new SaveFileDialog())
            {
                // Set the file name to a variable
                string filename = "";

                // Show only text files to write out to
                sfd.Filter = "Text files (*.txt)|*.txt";

                // Change the title of the save dialog 
                sfd.Title = "Save a Text File";

                // Show the dialog for save
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    // Change the filename variable to the file
                    filename = sfd.FileName.ToString();

                    if (filename != "")
                    {
                        // Start using the stream writer with the file name of the user's choosing
                        using (StreamWriter sw = new StreamWriter(filename))
                        {
                            // Add a header to the beginning to the text file
                            sw.WriteLine(string.Format("{0, -7} {1, -20}","|Have|", "|Item Name|\r\n" +
                                "*******************"));

                            // Loop through the have list to save out 
                            foreach (ListViewItem item in lvHave.Items)
                            {
                                // Write the have list out to the text file
                                sw.WriteLine(string.Format("{0, -10} {1, -20}", "HAVE", item.SubItems[0].Text));
                            }

                            // Loop through the need list to save out
                            foreach (ListViewItem item in lvNeed.Items)
                            {
                                // Write the need list out to the text file
                                sw.WriteLine("{0, -10} {1, -20}", "NEED", item.SubItems[0].Text);
                            }
                        }
                    }
                }
            }
        }

        // --------------------------------------------------------------------------------------------------
        // Load file method
        // --------------------------------------------------------------------------------------------------
        public void LoadFile()
        {
            // Open dialog new instance
            OpenFileDialog openDialog = new OpenFileDialog();

            // Change the title of the open dialog
            openDialog.Title = "Open Item Text File";

            // Filter out just text files to open
            openDialog.Filter = "TXT files|*.txt";

            // Show the initial directory when the open dialog opens up
            openDialog.InitialDirectory = @"C:\";

            // If statement for the opening of the open dialog
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                // Clear all lists so no duplicates arise
                lvHave.Items.Clear();
                lvNeed.Items.Clear();

                // Save file name as a string
                string fileName = openDialog.FileName;

                // Create an array of all the lines in the text box
                string[] filelines = File.ReadAllLines(fileName);
                
                // Set lines of the file to the length of the file
                int linesPerItem = filelines.Length;

                // Start at the index 2 because of headers
                int currItemLine = 2;

                // Parse line by line into instance of items class
                Items items = new Items();
                for (int a = 0; a < filelines.Length; a++)
                {
                    // Try method to not throw an error
                    try
                    {
                        // Check to see if the item goes in the have list
                        if (filelines[currItemLine].Substring(0, filelines[currItemLine].IndexOf(" ")) == "HAVE")
                        {
                            // Cut out the 'have' in the beginning
                            if (filelines[currItemLine].Length > 0)
                            {
                                int i = filelines[currItemLine].IndexOf(" ") + 1;

                                // Trim the beginning and trailing spaces
                                string str = filelines[currItemLine].Substring(i).TrimStart().TrimEnd();
                                
                                // Add object to the list
                                lvHave.Items.Add(str).ToString();
                            }

                            // Index the current line item to the next one
                            currItemLine++;

                            // Reset all of the inputs
                            ResetInputs();
                        }
                        // Check to see if the item goes in the need list
                        else if (filelines[currItemLine].Substring(0, filelines[currItemLine].IndexOf(" ")) == "NEED")
                        {
                            // Cut out the 'need' in the beginning
                            if (filelines[currItemLine].Length > 0)
                            {
                                int i = filelines[currItemLine].IndexOf(" ") + 1;

                                // Trim the beginning and trailing spaces
                                string str = filelines[currItemLine].Substring(i).TrimStart().TrimEnd();
                                
                                // Add the object to the list
                                lvNeed.Items.Add(str).ToString();
                            }

                            // Index the current line item to the next one
                            currItemLine++;

                            // Reset all of the inputs
                            ResetInputs();
                        }
                    }
                    // Catch method to not throw any errors
                    catch
                    {
                        // Reset all of the inputs
                        ResetInputs();

                        // Return to the application
                        return;
                    }
                }
            }
        }

        // --------------------------------------------------------------------------------------------------
        // Reset method
        // --------------------------------------------------------------------------------------------------
        public void ResetInputs()
        {
            btnDelete.Enabled = false;
            btnToHave.Enabled = false;
            btnToNeed.Enabled = false;
        }
        
        // --------------------------------------------------------------------------------------------------
        // Selection method
        // --------------------------------------------------------------------------------------------------
        public void SelectionChange()
        {
            // Fill in the data field with the selected item
            if (lvHave.SelectedItems.Count > 0)
            {
                // Show what buttons the user can use
                btnToNeed.Enabled = true;
                btnDelete.Enabled = true;
            }
            else if (lvNeed.SelectedItems.Count > 0)
            {
                // Show what buttons the user can use
                btnToHave.Enabled = true;
                btnDelete.Enabled = true;
            }
            else
            {
                ResetInputs();
            }
        }
    }
}
