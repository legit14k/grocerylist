﻿namespace GrimbergJason_GroceryList
{
    partial class UserInputDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDataInput = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.gbHaveOrNeed = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.radToNeed = new System.Windows.Forms.RadioButton();
            this.radToHave = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbDataInput.SuspendLayout();
            this.gbHaveOrNeed.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDataInput
            // 
            this.gbDataInput.Controls.Add(this.label1);
            this.gbDataInput.Controls.Add(this.txtItemName);
            this.gbDataInput.Location = new System.Drawing.Point(12, 12);
            this.gbDataInput.Name = "gbDataInput";
            this.gbDataInput.Size = new System.Drawing.Size(361, 65);
            this.gbDataInput.TabIndex = 14;
            this.gbDataInput.TabStop = false;
            this.gbDataInput.Text = "Data Input";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Item Name: ";
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(76, 19);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(219, 20);
            this.txtItemName.TabIndex = 0;
            // 
            // gbHaveOrNeed
            // 
            this.gbHaveOrNeed.Controls.Add(this.btnCancel);
            this.gbHaveOrNeed.Controls.Add(this.btnAdd);
            this.gbHaveOrNeed.Controls.Add(this.radToNeed);
            this.gbHaveOrNeed.Controls.Add(this.radToHave);
            this.gbHaveOrNeed.Controls.Add(this.label3);
            this.gbHaveOrNeed.Location = new System.Drawing.Point(53, 83);
            this.gbHaveOrNeed.Name = "gbHaveOrNeed";
            this.gbHaveOrNeed.Size = new System.Drawing.Size(271, 108);
            this.gbHaveOrNeed.TabIndex = 15;
            this.gbHaveOrNeed.TabStop = false;
            this.gbHaveOrNeed.Text = "Have Or Need";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(156, 79);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(98, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Add to List";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // radToNeed
            // 
            this.radToNeed.AutoSize = true;
            this.radToNeed.Location = new System.Drawing.Point(121, 44);
            this.radToNeed.Name = "radToNeed";
            this.radToNeed.Size = new System.Drawing.Size(96, 17);
            this.radToNeed.TabIndex = 2;
            this.radToNeed.TabStop = true;
            this.radToNeed.Text = "I need this item";
            this.radToNeed.UseVisualStyleBackColor = true;
            // 
            // radToHave
            // 
            this.radToHave.AutoSize = true;
            this.radToHave.Location = new System.Drawing.Point(19, 44);
            this.radToHave.Name = "radToHave";
            this.radToHave.Size = new System.Drawing.Size(96, 17);
            this.radToHave.TabIndex = 1;
            this.radToHave.TabStop = true;
            this.radToHave.Text = "I have this item";
            this.radToHave.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Do you have this item or do you need this item?";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(19, 79);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // UserInputDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 201);
            this.ControlBox = false;
            this.Controls.Add(this.gbHaveOrNeed);
            this.Controls.Add(this.gbDataInput);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserInputDialog";
            this.Text = "UserInputDialog";
            this.gbDataInput.ResumeLayout(false);
            this.gbDataInput.PerformLayout();
            this.gbHaveOrNeed.ResumeLayout(false);
            this.gbHaveOrNeed.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDataInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.GroupBox gbHaveOrNeed;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.RadioButton radToNeed;
        public System.Windows.Forms.RadioButton radToHave;
    }
}