﻿namespace GrimbergJason_GroceryList
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuInputForm = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusInputForm = new System.Windows.Forms.StatusStrip();
            this.btnToHave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnToNeed = new System.Windows.Forms.Button();
            this.lvNeed = new System.Windows.Forms.ListView();
            this.lvHave = new System.Windows.Forms.ListView();
            this.lblNeed = new System.Windows.Forms.Label();
            this.lblHave = new System.Windows.Forms.Label();
            this.gbGroceryList = new System.Windows.Forms.GroupBox();
            this.menuInputForm.SuspendLayout();
            this.gbGroceryList.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuInputForm
            // 
            this.menuInputForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuInputForm.Location = new System.Drawing.Point(0, 0);
            this.menuInputForm.Name = "menuInputForm";
            this.menuInputForm.Size = new System.Drawing.Size(596, 24);
            this.menuInputForm.TabIndex = 0;
            this.menuInputForm.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNew,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // menuNew
            // 
            this.menuNew.Name = "menuNew";
            this.menuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuNew.Size = new System.Drawing.Size(180, 22);
            this.menuNew.Text = "&New Item";
            this.menuNew.Click += new System.EventHandler(this.menuNew_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadToolStripMenuItem.Text = "&Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // statusInputForm
            // 
            this.statusInputForm.Location = new System.Drawing.Point(0, 548);
            this.statusInputForm.Name = "statusInputForm";
            this.statusInputForm.Size = new System.Drawing.Size(596, 22);
            this.statusInputForm.TabIndex = 1;
            this.statusInputForm.Text = "statusStrip1";
            // 
            // btnToHave
            // 
            this.btnToHave.Location = new System.Drawing.Point(258, 203);
            this.btnToHave.Name = "btnToHave";
            this.btnToHave.Size = new System.Drawing.Size(55, 23);
            this.btnToHave.TabIndex = 9;
            this.btnToHave.Text = "<";
            this.btnToHave.UseVisualStyleBackColor = true;
            this.btnToHave.Click += new System.EventHandler(this.btnToHave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(258, 174);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(55, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnToNeed
            // 
            this.btnToNeed.Location = new System.Drawing.Point(258, 145);
            this.btnToNeed.Name = "btnToNeed";
            this.btnToNeed.Size = new System.Drawing.Size(55, 23);
            this.btnToNeed.TabIndex = 11;
            this.btnToNeed.Text = ">";
            this.btnToNeed.UseVisualStyleBackColor = true;
            this.btnToNeed.Click += new System.EventHandler(this.btnToNeed_Click);
            // 
            // lvNeed
            // 
            this.lvNeed.HideSelection = false;
            this.lvNeed.Location = new System.Drawing.Point(320, 32);
            this.lvNeed.MultiSelect = false;
            this.lvNeed.Name = "lvNeed";
            this.lvNeed.Size = new System.Drawing.Size(243, 478);
            this.lvNeed.TabIndex = 7;
            this.lvNeed.UseCompatibleStateImageBehavior = false;
            this.lvNeed.View = System.Windows.Forms.View.List;
            this.lvNeed.SelectedIndexChanged += new System.EventHandler(this.lvNeed_SelectedIndexChanged);
            // 
            // lvHave
            // 
            this.lvHave.HideSelection = false;
            this.lvHave.Location = new System.Drawing.Point(9, 32);
            this.lvHave.MultiSelect = false;
            this.lvHave.Name = "lvHave";
            this.lvHave.Size = new System.Drawing.Size(243, 478);
            this.lvHave.TabIndex = 8;
            this.lvHave.UseCompatibleStateImageBehavior = false;
            this.lvHave.View = System.Windows.Forms.View.List;
            this.lvHave.SelectedIndexChanged += new System.EventHandler(this.lvHave_SelectedIndexChanged);
            // 
            // lblNeed
            // 
            this.lblNeed.AutoSize = true;
            this.lblNeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNeed.Location = new System.Drawing.Point(317, 16);
            this.lblNeed.Name = "lblNeed";
            this.lblNeed.Size = new System.Drawing.Size(45, 13);
            this.lblNeed.TabIndex = 6;
            this.lblNeed.Text = "Need: ";
            // 
            // lblHave
            // 
            this.lblHave.AutoSize = true;
            this.lblHave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHave.Location = new System.Drawing.Point(6, 16);
            this.lblHave.Name = "lblHave";
            this.lblHave.Size = new System.Drawing.Size(45, 13);
            this.lblHave.TabIndex = 5;
            this.lblHave.Text = "Have: ";
            // 
            // gbGroceryList
            // 
            this.gbGroceryList.Controls.Add(this.lblHave);
            this.gbGroceryList.Controls.Add(this.btnToHave);
            this.gbGroceryList.Controls.Add(this.lblNeed);
            this.gbGroceryList.Controls.Add(this.btnDelete);
            this.gbGroceryList.Controls.Add(this.lvHave);
            this.gbGroceryList.Controls.Add(this.btnToNeed);
            this.gbGroceryList.Controls.Add(this.lvNeed);
            this.gbGroceryList.Location = new System.Drawing.Point(12, 27);
            this.gbGroceryList.Name = "gbGroceryList";
            this.gbGroceryList.Size = new System.Drawing.Size(572, 516);
            this.gbGroceryList.TabIndex = 12;
            this.gbGroceryList.TabStop = false;
            this.gbGroceryList.Text = "Grocery List";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 570);
            this.Controls.Add(this.gbGroceryList);
            this.Controls.Add(this.statusInputForm);
            this.Controls.Add(this.menuInputForm);
            this.MainMenuStrip = this.menuInputForm;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(612, 609);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(612, 609);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input Form";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuInputForm.ResumeLayout(false);
            this.menuInputForm.PerformLayout();
            this.gbGroceryList.ResumeLayout(false);
            this.gbGroceryList.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuInputForm;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusInputForm;
        private System.Windows.Forms.Button btnToHave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnToNeed;
        private System.Windows.Forms.ListView lvNeed;
        private System.Windows.Forms.ListView lvHave;
        private System.Windows.Forms.Label lblNeed;
        private System.Windows.Forms.Label lblHave;
        private System.Windows.Forms.GroupBox gbGroceryList;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuNew;
    }
}

