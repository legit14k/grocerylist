﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrimbergJason_GroceryList
{
    public partial class UserInputDialog : Form
    {
        // Creating a new instance of the main window
        MainForm main = new MainForm();

        // --------------------------------------------------------------------------------------------------
        // EventHandler call
        // --------------------------------------------------------------------------------------------------
        public EventHandler ObjectAdded;

        public UserInputDialog()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------------------
        // Item data components
        // --------------------------------------------------------------------------------------------------
        // New item data
        public Items Data
        {
            // Get all the item data
            get
            {
                Items i = new Items();
                i.ItemName = txtItemName.Text;
                i.ToHave = radToHave.Checked;
                i.ToNeed = radToNeed.Checked;
                return i;
            }

            // Set all the item data
            set
            {
                txtItemName.Text = value.ItemName;
                radToHave.Checked = value.ToHave;
                radToNeed.Checked = value.ToNeed;
            }
        }
        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Creating a new instance of the main window
            main = new MainForm();

            // Subscribing to the new added handler
            ObjectAdded += main.ObjectAddedHandler;
            
            if(txtItemName.Text != "")
            {
                // Call add method
                AddToList();
            }

            Data = new Items();
            this.Close();
        }
        // --------------------------------------------------------------------------------------------------
        // Add item method
        // --------------------------------------------------------------------------------------------------
        public void AddToList()
        {
            if (ObjectAdded != null)
            {
                ObjectAdded(this, new EventArgs());
            }
        }

        // Close out of this dialog
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
