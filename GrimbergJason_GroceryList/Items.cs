﻿namespace GrimbergJason_GroceryList
{
    public class Items
    {
        // Add variables
        string itemName;
        bool toHave;
        bool toNeed;

        // Encapsulate the item name
        public string ItemName { get => itemName; set => itemName = value; }
        public bool ToHave { get => toHave; set => toHave = value; }
        public bool ToNeed { get => toNeed; set => toNeed = value; }

        // Override string
        public override string ToString()
        {
            return ItemName.ToString();
        }
    }
}
